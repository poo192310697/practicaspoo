﻿using System;

namespace PRACTICA01._1
{
    class Program
    {
        class Operacion
        {
            private int[,] mat;

            public void Datos()
            {
                Console.Write("Ingrese las filas que tiene la matriz:");
                string 
                    n;
                n = Console.ReadLine();
                int filas = int.Parse(n);
                Console.Write("Ingrese las columnas que tiene la matriz:");
                n = Console.ReadLine();
                int columnas = int.Parse(n);
                mat = new int[filas, columnas];
                for (int f = 0; f < mat.GetLength(0); f++)
                {
                    for (int c = 0; c < mat.GetLength(1); c++)
                    {
                        Console.Write("Ingrese los componente de la matriz:");
                        n = Console.ReadLine();
                        mat[f, c] = int.Parse(n);
                    }
                }
            }

            public void LeerMatriz()
            {
                for (int f = 0; f < mat.GetLength(0); f++)
                {
                    for (int c = 0; c < mat.GetLength(1); c++)
                    {
                        Console.Write(mat[f, c] + " ");
                    }
                    Console.WriteLine();
                }
            }

            public void ImprimeMatriz()
            {
                Console.WriteLine("Ultima fila");
                for (int c = 0; c < mat.GetLength(1); c++)
                {
                    Console.Write(mat[mat.GetLength(0) - 1, c] + " ");
                }
            }

            static void Main(string[] args)
            {
                Operacion a = new Operacion();
                a.Datos();
                a.LeerMatriz();
                a.ImprimeMatriz();
                Console.ReadKey();
            }
        }
    }
}
