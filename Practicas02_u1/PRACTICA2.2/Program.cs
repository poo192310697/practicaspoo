﻿using System;

namespace PRACTICA2._2
{
    class Program
    {
        class Cliente
        {
            private string apellidomaterno;
            private string apellidopaterno;
            private double cp;
            private string direccion;
            private double idcliente;
            private string nombre;
            private double telefono;
            private double telefonocasa;
            private double telefonomovil;
            private string nuevo;

            public void inicializar()
            {
                Console.WriteLine("Cual es su apellido materno");
                apellidomaterno = Console.ReadLine();
                Console.WriteLine("Cual es su apellido paterno");
                apellidopaterno = Console.ReadLine();
                Console.WriteLine("Cual es su CP");
                cp = double.Parse(Console.ReadLine());
                Console.WriteLine("Cual es su direccion");
                direccion = (Console.ReadLine());
                Console.WriteLine("Cual es su IDCliente");
                idcliente = double.Parse(Console.ReadLine());
                Console.WriteLine("Cual es su nombre");
                nombre = (Console.ReadLine());
                Console.WriteLine("Cual es su telefono");
                telefono = double.Parse(Console.ReadLine());
                Console.WriteLine("Cual es su telefono de casa");
                telefonocasa = double.Parse(Console.ReadLine());
                Console.WriteLine("Cual es su telefono movil");
                telefonomovil = double.Parse(Console.ReadLine());
            }
            public void EliminaCliente()
            {
                cp = 0;
                idcliente = 0;
                
                Console.WriteLine("Usted ha sido eliminado del sistema");
            }
            public void InsertarCliente()
            {
                
                nuevo = apellidopaterno + apellidomaterno + nombre;
                Console.WriteLine("Usted ha sido insertado en el sistema" + nuevo);
            }
            public void MostrarCliente()
            {
                telefono = 0;
                telefonocasa = 0;
                telefonomovil = 0;
                Console.WriteLine("Usted ha sido mostrado en el sistema");
            }
            static void Main(string[] args)
            {
                Cliente o = new Cliente();
                o.inicializar();
                o.EliminaCliente();
                o.InsertarCliente();
                o.MostrarCliente();
            }
            
        }   
    }
}
